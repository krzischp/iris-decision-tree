# "Automating" model update
In `mlserver` repository

```bash
git clone https://gitlab.com/krzischp/mlserver.git
mkdir mlserver/models/iris-decision-tree-v0.0.1
cp iris-decision-tree.joblib mlserver/models/iris-decision-tree-v0.0.1
cp model-settings.json mlserver/models/iris-decision-tree-v0.0.1
cd mlserver
git add .
git commit -m "Releasing model iris-decision-tree-v0.0.1"
git push
cd ..
rm -rf mlserver
```

# Papermill
```bash
$HOME/.pyenv/versions/3.9.6/envs/classificador-produtos/bin/pip install -r requirements.txt
```
```bash
$HOME/.pyenv/versions/3.9.6/envs/classificador-produtos/bin/papermill iris-decision-tree.ipynb -
```
A opção `-` no final diz ao papermill para exibir o resultado da execução no próprio terminal.


É possível rodar um comando bash que substitui a string `$VERSAO` por uma qualquer no arquivo `model-settings.json`. Em um terminal bash execute:
```bash
sed -i 's/$VERSAO/v1.4.5/' model-settings.json
```

# Automatizando a clonagem e configuração do projeto do MLServer
- Acesse o GitLab
- Clique no ícone para editar seu perfil, no canto superior direito da página:
- Em seguida, clique em "Access Tokens":
- Na página que aparecer, escolha o nome "access-mlserver" para seu token, ative a opção `write_repository`, e crie o token. Se quiser definir uma data para que o token expire, é possível. Caso deixe em branco, o token terá validade indeterminada.
- Assim que o processo for concluído, será exibido o token criado. Conforme as instruções, copie-o agora e salve-o em algum local seguro, pois o mesmo não poderá ser visto novamente assim que sair da página (será necessário revogá-lo e criar novamente).


Também é necessário definir o e-mail e nome de usuário, assim não é necessário ter mais nenhuma configuração extra.
```bash
git config --global user.email "pekrzisch.pek@gmail.com"
git config --global user.name "Pierre Krzisch"
git clone https://access-mlserver:xxxxxxxxxxxxxxxxxx@gitlab.com/krzischp/mlserver.git
mkdir mlserver/models/iris-decision-tree-v0.0.2
cp iris-decision-tree.joblib mlserver/models/iris-decision-tree-v0.0.2
cp model-settings.json mlserver/models/iris-decision-tree-v0.0.2
cd mlserver
git add .
git commit -m "Releasing model iris-decision-tree-v0.0.2"
git push
cd ..
rm -rf mlserver
```


# Pipeline execution

**Commit sem executar o pipeline**
```bash
git add .
git commit -m "Adicionando pipeline CI/CD"
git push
```

**Commit com execuçao do pipeline**
```bash
git tag -a v1.0.0 -m "Primeiro release"
git push --tags
```

- O notebook foi enviado ao GitLab, mas sem o arquivo do modelo, lembra que colocamos no .gitignore?
- O GitLab vai subir um contêiner Docker, com base na imagem Python que configuramos
- O GitLab vai instalar as dependências todas, incluindo o papermill
- O GitLab vai rodar o notebook usando o papermill. Isso irá gerar o arquivo .joblib
- O GitLab vai clonar o repositório do MLServer, onde estão todos os modelos
- O GitLab vai modificar o arquivo model-settings.json para marcá-lo com a versão especificada na tag (neste exemplo: "v1.0.0)
- O GitLab vai criar a estrutura para esse novo modelo, criando uma pasta para ele e copiando os dois arquivos
- O GitLab vai enviar as mudanças para o repositório do MLServer, em um commit
- Já no repositório do MLServer, o GitLab vai iniciar outro pipeline, associado ao commit
- Nesse pipeline, o GitLab vai subir um contêiner Docker que roda Docker (Docker-in-Docker)
- O GitLab vai instalar as ferramentas do Heroku
- O GitLab vai construir uma imagem para o MLServer, já com o novo modelo dentro
- O GitLab vai se autenticar no Heroku e vai enviar a imagem para o Docker registry do Heroku
- O GitLab vai pedir ao Heroku para fazer o release da nova imagem

# Tag protegida:
 
```bash
git tag -a 1.0.0 -m "Uma tag nao protegida" | git push --tags
```
O pipeline irá rodar, mas ele irá falhar, pois essa tag não é protegida e portanto as variáveis necessárias para a execução dos jobs não estarão disponíveis.

Agora com uma tag protegida:
```bash
git tag -a v1.0.1 -m "Testando variáveis em tag protegida" | git push --tags
```
Agora tudo irá funcionar normalmente.

